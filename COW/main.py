import numpy as np
import re

with open('hello.cow', 'r') as file:
    content = file.read()

commands = re.sub(r'\n', r' ', content).split(' ')

stack = []
dictionary = {}
current_index = 0
result = np.zeros(1000)

for command in commands:
    if command == 'MOO':
        stack.append(current_index)
    elif command == 'moo':
        dictionary[current_index] = stack[-1]
        dictionary[stack.pop()] = current_index
    current_index += 1

index = 0
i = 0

while i != len(commands):
    if commands[i] == 'MoO':
        result[index] += 1
    elif commands[i] == 'MOo':
        result[index] -= 1
    elif commands[i] == 'moO':
        index += 1
    elif commands[i] == 'mOo':
        index -= 1
    elif commands[i] == 'OOM':
        print(int(result[index]), end='')
    elif commands[i] == 'Moo':
        if result[index] != 0:
            print(chr(int(result[index])), end='')
        else:
            input("Enter your value")
    elif commands[i] == 'OOO':
        result[index] = 0
    elif commands[i] == 'moo':
        i = dictionary[i] - 1
    elif commands[i] == 'MOO':
        if result[index] == 0:
            i = dictionary[i]
    elif commands[i] == '':
        pass
    i += 1
